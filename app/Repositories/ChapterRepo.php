<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ChapterRepo {
    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' => $data
        ]);
    }

    public function getChapters($directory_id) {
        try {
            $chapters = DB::table('chapters')->where('directory_id', $directory_id)->get();
            if(is_null($chapters) || count($chapters) == 0) {
                return $this->sendResponse(true, array());
            }
            return $this->sendResponse(false, $chapters);
        } catch (QueryException $e) {
            return $this->sendResponse(true, array());
        } catch (\Exception $e) {
            return $this->sendResponse(true, array());
        }
    }
}
