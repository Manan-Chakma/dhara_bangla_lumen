<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class GalleryRepository {
    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' => $data
        ]);
    }

    public function getGalleryData($class_id) {
        try {
            $gallery = DB::table('gallery')->where('class_id', $class_id)->get();

            if (is_null($gallery) || count($gallery) == 0) {
                return $this->sendResponse(true, array());
            }
            return $this->sendResponse(false, $gallery);
        } catch (QueryException $e) {
            return $this->sendResponse(true, array());
        } catch (\Exception $e) {
            return $this->sendResponse(true, array());
        }
    }
}
