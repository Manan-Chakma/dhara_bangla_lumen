<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepo {


    public function insertUser($name, $class_id, $inst_name, $password, $phone) {
        try {
            DB::table('users')->insert([
                'name' => $name,
                'institution_name' => $inst_name,
                'password' => $password,
                'phone' => $phone,
                'class_id' => $class_id,
            ]);
        } catch (QueryException $e) {
            $this->sendResponse(true, array());
        } catch (\Exception $e) {
            $this->sendResponse(true, array());
        }
        return $this->sendResponse(false, array());
    }

    public function userExists($phone) {
        try {
            $user = DB::table('users')->select('phone')->where('phone', $phone)->first();
            if (is_null($user)) {
                return false;
            }
        } catch (QueryException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
        return true;

    }

    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' =>$data
        ]);
    }
}

