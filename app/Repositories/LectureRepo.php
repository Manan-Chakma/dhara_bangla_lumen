<?php

namespace App\Repositories;

use App\Http\Resources\LectureResource;
use Illuminate\Support\Facades\DB;

class LectureRepo {

    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' => $data
        ]);
    }

    public function getLectures($chapter_id) {
        $lectures = DB::table('lectures')->where('chapter_id', $chapter_id)->get();

        $data = LectureResource::collection($lectures);

        if(is_null($lectures) || count($lectures) == 0 ){
            return $this->sendResponse(true, array());
        }
        return $this->sendResponse(false, $data);
    }
}
