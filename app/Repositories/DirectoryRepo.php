<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class DirectoryRepo {
    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' => $data
        ]);
    }



//    public function directories($class_id) {
//        try {
//            $directories = DB::table('directories')->where('class_id', $class_id)->get();
//        } catch (QueryException $e) {
//            return $this->sendResponse(true, array());
//        } catch (\Exception $e) {
//            return $this->sendResponse(true, array());
//        }
//
//        $result = [];
//
//        foreach ($directories as $directory) {
//            if ($directory->parent !== 0) {
//                $parentId = $directory->parent;
//                $result[$parentId]->has_sub_directory = true;
//
//                if(! isset($result[$parentId]->sub_directory)) {
//                    $result[$parentId]->sub_directory = array();
//                }
//                $sub_dir_arr = $result[$parentId]->sub_directory;
//                $sub_dir_arr[$directory->id] = $directory;
//                $sub_dir_arr[$directory->id]->has_sub_directory = false;
//                $result[$parentId]->sub_directory = $sub_dir_arr;
//
//
//            } else {
//                $result[$directory->id] = $directory;
//                $result[$directory->id]->has_sub_directory = false;
//            }
//        }
//        return $this->sendResponse(false, $result);
//    }



    public function getDirectories($class_id) {
        try {
            $directories = DB::table('directories')->where('class_id', $class_id)->get();
        } catch (QueryException $e) {
            return $this->sendResponse(true, array());
        } catch (\Exception $e) {
            return $this->sendResponse(true, array());
        }

        $result = [];

        foreach ($directories as $directory) {
            if ($directory->parent !== 0) {
                $parentId = $directory->parent;

                $track = [];
                // get the parents // 4-> 2, 1
                while ($parentId != 0) {
                    array_push($track, $parentId);
                    $parentId = $directories->where('id', $parentId)->first()->parent;
                }
                //return $this->sendResponse(false, $parentId);
                // 1, 2
                $track = array_reverse($track);

                // iterate through the result array by passing the reference
                $this->setDirectory($result, $directory, $directory->id, $track, 0);

            } else {
                $result[$directory->id] = $directory;
                $result[$directory->id]->has_sub_directory = false;
            }
        }
        return $this->sendResponse(false, $result);
    }

    private function setDirectory( &$result, $directory, $id, $track, $i) {
        if($i >= count($track)) {
            $result[$id] = $directory;
            $result[$id]->has_sub_directory = false;
        } else {
            $this->setDirectory( $result[$track[$i]]->sub_directory, $directory, $id, $track, $i+1);
        }

    }

}
