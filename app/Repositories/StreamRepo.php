<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class StreamRepo {
    public function sendResponse($isError, $data) {
        return response()->json([
            'error' => $isError,
            'success' => !$isError,
            'data' => $data
        ]);
    }

    public function getStream($lecture_id, $chapter_id) {
        try {
            $streams = DB::table('streams')->where([
                ['lecture_id', '=', $lecture_id],
                ['chapter_id', '=', $chapter_id]
            ])->get();
        } catch (QueryException $exception) {
            return $this->sendResponse(true, array());
        } catch (\Exception $e) {
            return $this->sendResponse(true, array());
        }
        if (is_null($streams) || count($streams) <= 0) {
            return $this->sendResponse(true, array());
        }
        return $this->sendResponse(false, $streams);
    }

    public function getStreamUrl($lecture_id, $chapter_id) {
        try {
            $streams = DB::table('streams')
                ->select('stream_url', 'stream_type')
                ->where([
                ['lecture_id', '=', $lecture_id],
                ['chapter_id', '=', $chapter_id]
            ])->get();

//            $result = $streams->mapWithKeys(function ($item){
//                return [$item['stream_type'] => $item['stream_url']];
//            });
        } catch (QueryException $exception) {
            return $this->sendResponse(true, array());
        } catch (\Exception $e) {
            return $this->sendResponse(true, array());
        }
        if (is_null($streams)) {
            return $this->sendResponse(true, array());
        }
        return $streams;
    }
}
