<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller {

//    public function getPhoneNumber($phone){
//        $str = "+88";
//        $data = array();
//
//        if(strpos($phone, $str) !== false){
//            $data["ext_phone"] = $phone;
//            $data['phone'] = str_replace("+88","",$phone);
//        }else{
//            $data["ext_phone"] = "+88".$phone;
//            $data["phone"] = $phone;
//        }
//
//        return $data;
//    }

    public function loginWithOtp(Request $request) {
        if (!$this->mobileNoExists($request->phoneNo) || !$this->isValid($request->phoneNo)) {
            // open an account first
        } else {
            $this->sendOtp($request);
        }

    }

    public function isValid($phoneNo) {
        return true;
    }

    public function mobileNoExists($phoneNo) {
        try {
            $user = DB::table('users')->where('mobile', $phoneNo)->get();
            if (is_null($user)) {
                return false;
            }
            return true;

        } catch (QueryException $exception) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function verifyOtp(Request $request) {
        try {

            $otp_pass = DB::table('otp')->select('otp', 'created_at')->where('mobile', $request->model)->get();
            if ($otp_pass->otp != $request->otp || Carbon::now()->diffInMinutes(Carbon::parse($otp_pass->created_at)) > 10) {
                // do not match // time out
            } else {
                // login
            }

        } catch (QueryException $e) {
            // query exeption
        } catch (\Exception $e) {
            // exception //
        }


    }

    public function resendOtp(Request $request) {
        $this->sendOtp($request);
    }

    public function sendOtp(Request $request) {
        $otp = rand(1000, 9999);
        Log::info("otp".$otp);
        try {
            // insert otp into db
            DB::table('otp')->updateOrInsert(
                ['mobile' => $request->mobile],
                ['otp' => $otp, 'created_at' => Carbon::now()]
            );
        } catch (QueryException $e) {
            // failed to send otp
        } catch (\Exception $e) {
            // failed to send otp
        }

    }
}
