<?php

namespace App\Http\Controllers;

use App\Repositories\LectureRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LectureController extends Controller {
    protected $repo;

    public function __construct(LectureRepo $repo) {
        $this->repo = $repo;
    }

    public function show(Request $request) {
        $data = json_decode($request->getContent(), true);
        $chapter_id = $data['chapter_id'];

        if(is_null($chapter_id)) {
            return $this->repo->sendResponse(true,  array());
        }
        return $this->repo->getLectures($chapter_id); // get lectures by chapter id
    }
}
