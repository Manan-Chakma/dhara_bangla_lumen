<?php

namespace App\Http\Controllers;

use App\Repositories\DirectoryRepo;
use Illuminate\Http\Request;

class DirectoryController extends Controller {

    protected $repo;

    public function __construct(DirectoryRepo $repo) {
        $this->repo = $repo;
    }


    // input - (class id)   output - (directories under that class id)
    public function show(Request $request) {
        $data = json_decode($request->getContent(), true);
        $class_id = $data['class_id']; // get class id
        if(is_null($class_id)) {
            return $this->repo->sendResponse(true, array());
        }

        return $this->repo->getDirectories($class_id);// get directories by class id
    }


}
