<?php

namespace App\Http\Controllers;

use App\Repositories\ChapterRepo;
use Illuminate\Http\Request;

class ChapterController extends Controller {

    protected $repo;

    public function __construct(ChapterRepo $repo) {
        $this->repo = $repo;
    }



    public function show(Request $request) {
        $data = json_decode($request->getContent(), true);
        $directory_id = $data['directory_id'];
        if(is_null($directory_id)) {
            return $this->repo->sendResponse(true,  array());
        }
        return $this->repo->getChapters($directory_id); // get chapters by directory id
    }

}
