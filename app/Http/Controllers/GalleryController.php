<?php

namespace App\Http\Controllers;

use App\Repositories\GalleryRepository;
use Illuminate\Http\Request;

class GalleryController extends Controller {
    protected $repo;

    public function __construct(GalleryRepository $repo) {
        $this->repo = $repo;
    }

    public function show(Request $request) {
        $data = json_decode($request->getContent(), true);
        $class_id = $data['class_id']; // get class id
        if (is_null($class_id)) {
            return $this->repo->sendResponse(true, array());
        }
        return $this->repo->getGalleryData($class_id);// get gallery data by class id
    }
}
