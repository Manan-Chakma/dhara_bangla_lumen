<?php

namespace App\Http\Controllers;

use App\Repositories\StreamRepo;
use Illuminate\Http\Request;

class StreamController extends Controller {
    protected $repo;

    public function __construct(StreamRepo $repo) {
        $this->repo = $repo;
    }

    public function show(Request $request) {
        $data = json_decode($request->getContent(), true);
        $chapter_id = $data['chapter_id'];
        $lecture_id = $data['lecture_id'];

        if(is_null($chapter_id) || is_null($lecture_id)) {
            return $this->repo->sendResponse(true,  array());
        }
        return $this->repo->getStream($lecture_id, $chapter_id); // get stream by lecture and chapter id
    }

    public function getStreamUrl($lecture_id, $chapter_id) {

        if(is_null($chapter_id) || is_null($lecture_id)) {
            return $this->repo->sendResponse(true,  array());
        }
        return $this->repo->getStreamUrl($lecture_id, $chapter_id); // get stream url by chapter id and lecture id
    }
}
