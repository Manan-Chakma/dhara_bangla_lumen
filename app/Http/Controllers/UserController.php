<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepo;
use Illuminate\Http\Request;

class UserController extends Controller {

    protected $repo;

    public function __construct(UserRepo $repo) {
        $this->repo = $repo;
    }

    public function index() {
        //
    }


    public function create() {
        //
    }


    public function store(Request $request) {

        $data = json_decode($request->getContent(), true);
        $name = $data['name'];
        $class_id = $data['class_id'];
        $inst_name = $data['institution_name'];
        $password = $data['password'];
        $phone = $data['phone'];
        // check null -- add
        if (is_null($name) || is_null($class_id) || is_null($inst_name) || is_null($password) || is_null($phone)|| (strlen($password) < 6) ) {
            return $this->repo->sendResponse(true, array());
        }
        // format the phone number // still need c
        $country_code = "+88";
        if (!strpos($phone, $country_code)) {
            $phone = $country_code.$phone;
        }
        if (!(strlen($phone) > 10 && strlen($phone) <= 15)) {
            return $this->repo->sendResponse(true, array());
        }
        if (!$this->repo->userExists($phone)) {
            return $this->repo->insertUser($name, $class_id, $inst_name, $password, $phone);
        }
        return $this->repo->sendResponse(true, array());
    }


    public function show($id) {
        //
    }

//    public function getPhoneNumber($phone){
//        $str = "+88";
//        $data = array();
//
//        if(strpos($phone, $str) !== false){
//            $data["ext_phone"] = $phone;
//            $data['phone'] = str_replace("+88","",$phone);
//        }else{
//            $data["ext_phone"] = "+88".$phone;
//            $data["phone"] = $phone;
//        }
//
//        return $data;
//    }
    public function edit($id) {
        //
    }


    public function update(Request $request, $id) {
        //
    }


    public function destroy($id) {
        //
    }
}
