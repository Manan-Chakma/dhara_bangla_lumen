<?php

namespace App\Http\Resources;

use App\Http\Controllers\StreamController;
use App\Repositories\StreamRepo;
use Illuminate\Http\Resources\Json\JsonResource;

class LectureResource extends JsonResource {

    public function toArray($request) {
        $controller = new StreamController(new StreamRepo());

        $streamData = $controller->getStreamUrl($this->id, $this->chapter_id);

        return [
            'id' => $this->id,
            'lecture_name' =>$this->lecture_name,
            'lecture_description' => $this->lecture_description,
            'chapter_id' => $this->chapter_id,
            'streams' => $streamData
        ];
    }
}
