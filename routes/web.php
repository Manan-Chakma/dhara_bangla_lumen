<?php

/** @var \Laravel\Lumen\Routing\Router $router */



$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api/v1'], function() use($router){
    //$router->get('/users', 'UserController@index');
    $router->post('/users', ['as' => 'user_store', 'uses' => 'UserController@store']);

    $router->post('/directory', ['as' => 'directory_show', 'uses' => 'DirectoryController@show']); // directory api
    $router->post('/directory/chapters', ['as' => 'chapter_show', 'uses' => 'ChapterController@show']); // chapter api
    $router->post('/directory/chapters/lectures', ['as' => 'lecture_show', 'uses' => 'LectureController@show']); // lectures api
    $router->post('/directory/chapters/lectures/streams', ['as' => 'stream_show', 'uses' => 'StreamController@show']); // streams api


    $router->post('/gallery', ['as' => 'gallery_show', 'uses' => 'GalleryController@show']); // gallery api



    $router->get('/login', ['as' => 'loginWithOtp', 'uses' => 'LoginController@loginWithOtp']);

});
